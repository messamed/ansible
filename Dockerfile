FROM centos:7

ENV LC_CTYPE=en_US.UTF-8 
RUN echo $LC_CTYPE
RUN yum -y install epel-release && \
    yum -y upgrade && \
    yum -y install \
        jq \
        openssh \
        git \
        sudo \
        gettext \
        moreutils \
        sshpass \
        python3 \
        python3-pip && \
        python3 -m pip install -U pip 

RUN python3 -m pip install ansible==3.4.0




























# problem : can't find ansible executable 
#FROM centos:7

#RUN adduser ansible
#USER ansible
#WORKDIR /home/ansible

#RUN  yum check-update; \
   #  yum install -y gcc libffi-devel python3 epel-release; \
   #  yum install -y python3-pip; \
   # yum install -y wget; \
    # yum -y install  python3-devel krb5-devel krb5-workstation python3-devel;\
     #yum -y install python3-jmespath; \
  # yum clean all

#RUN python3 -m pip install --upgrade pip; \
   # python3 -m pip install --upgrade virtualenv; \
   # python3 -m pip install pywinrm[kerberos]; \
   # python3 -m pip install pywinrm; \
    #python3 -m pip install requests; \
    #python3 -m pip install ansible --no-deps 
